package br.com.theoldpinkeye.desafioconcrete;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.PersonViewHolder>{

    public static class PersonViewHolder extends RecyclerView.ViewHolder{

        CardView repo;
        TextView repoName;
        TextView description;
        TextView ownerName;
        TextView forksNumber;
        TextView starsNumber;
        ImageView starIcon;
        ImageView forkIcon;
        ImageView ownerAvatar;



        public PersonViewHolder(View itemView) {
            super(itemView);
            repo = (CardView) itemView.findViewById(R.id.repoCard);
            repoName = (TextView) itemView.findViewById(R.id.repoName);
            description = (TextView) itemView.findViewById(R.id.repoDescription);
            ownerName = (TextView) itemView.findViewById(R.id.ownerTextView);
            forksNumber = (TextView) itemView.findViewById(R.id.forksTextView);
            starsNumber = (TextView) itemView.findViewById(R.id.starsTextView);
            starIcon = (ImageView) itemView.findViewById(R.id.starIcon);
            forkIcon = (ImageView) itemView.findViewById(R.id.forkIcon);
            ownerAvatar = (ImageView) itemView.findViewById(R.id.ownerPhoto);




        }

    }
    private List<RepoInfo> repositorios = new ArrayList<>();



    public RecyclerViewAdapter(List<RepoInfo> repositorios){
        this.repositorios = repositorios;

    }

    public void refreshData(List<RepoInfo> repositorios){
        this.repositorios = repositorios;
        notifyDataSetChanged();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }


    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_item_layout, parent, false);
        PersonViewHolder pvh = new PersonViewHolder(view);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.PersonViewHolder holder, int position) {
        holder.ownerName.setText(repositorios.get(position).getOwner().getLogin());
        holder.repoName.setText(repositorios.get(position).getName());
        holder.description.setText((repositorios.get(position).getDescription()));
        holder.forksNumber.setText(Integer.toString(repositorios.get(position).getForks_count()));
        holder.starsNumber.setText(Integer.toString(repositorios.get(position).getStargazers_count()));
        holder.forkIcon.setImageResource(R.drawable.fork_icon);
        holder.starIcon.setImageResource(R.drawable.star_icon);
        Glide.with(holder.ownerAvatar)
                .load(repositorios.get(position).getOwner().getAvatar_url())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ownerAvatar);






    }

    @Override
    public int getItemCount() {
        return /*repositorios == null ? 0 : */repositorios.size();
    }





}

