package br.com.theoldpinkeye.desafioconcrete;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PullsRecyclerViewAdapter extends RecyclerView.Adapter<PullsRecyclerViewAdapter.PersonViewHolder>{

    public static class PersonViewHolder extends RecyclerView.ViewHolder{

        CardView pull;
        TextView pullName;
        TextView pullDescription;
        TextView ownerName;
        ImageView ownerAvatar;
        TextView creationDate;



        public PersonViewHolder(View itemView) {
            super(itemView);
            pull = (CardView) itemView.findViewById(R.id.pullRequestItemCard);
            pullName = (TextView) itemView.findViewById(R.id.pullReqName);
            pullDescription = (TextView) itemView.findViewById(R.id.pullDescription);
            ownerName = (TextView) itemView.findViewById(R.id.ownerNameTextView);
            ownerAvatar = (ImageView) itemView.findViewById(R.id.ownerAvatar);
            creationDate = (TextView) itemView.findViewById(R.id.creationDate);




        }

    }
    private List<PullInfo> pullReqs = new ArrayList<>();



    public PullsRecyclerViewAdapter(List<PullInfo> pullReqs){
        this.pullReqs = pullReqs;

    }

    public void refreshData(List<PullInfo> pullReqs){
        this.pullReqs = pullReqs;
        notifyDataSetChanged();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }


    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_request_item_layout, parent, false);
        PersonViewHolder pvh = new PersonViewHolder(layView);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PullsRecyclerViewAdapter.PersonViewHolder pvHolder, int position) {
        pvHolder.ownerName.setText(pullReqs.get(position).getUser().getLogin());
        pvHolder.pullName.setText(pullReqs.get(position).getTitle());
        pvHolder.pullDescription.setText((pullReqs.get(position).getBody()));

        Glide.with(pvHolder.ownerAvatar)
                .load(pullReqs.get(position).getUser().getAvatar_url())
                .apply(RequestOptions.circleCropTransform())
                .into(pvHolder.ownerAvatar);


        SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date dt = sd1.parse(pullReqs.get(position).getCreated_at());
            SimpleDateFormat sd2 = new SimpleDateFormat("dd/MM/yyyy");
            String newDate = sd2.format(dt);
            pvHolder.creationDate.setText(newDate);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("Deu problema!",pullReqs.get(position).getCreated_at());
        }




    }

    @Override
    public int getItemCount() {
        return /*pullReqs == null ? 0 : */pullReqs.size();
    }





}

