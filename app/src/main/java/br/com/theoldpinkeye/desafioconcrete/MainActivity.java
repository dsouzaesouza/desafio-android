package br.com.theoldpinkeye.desafioconcrete;

    import android.content.Intent;
    import android.graphics.Color;
    import android.os.AsyncTask;
    import android.support.v4.content.ContextCompat;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.support.v7.widget.LinearLayoutManager;
    import android.support.v7.widget.RecyclerView;
    import android.support.v7.widget.Toolbar;
    import android.util.Log;
    import android.view.View;
    import android.view.Window;
    import android.view.WindowManager;
    import com.chootdev.recycleclick.RecycleClick;
    import com.squareup.moshi.JsonAdapter;
    import com.squareup.moshi.Moshi;
    import com.squareup.moshi.Types;
    import org.apache.http.HttpResponse;
    import org.apache.http.client.methods.HttpGet;
    import org.apache.http.impl.client.DefaultHttpClient;
    import org.json.JSONObject;
    import java.io.BufferedReader;
    import java.io.InputStream;
    import java.io.InputStreamReader;
    import java.lang.reflect.Type;
    import java.util.ArrayList;
    import java.util.List;


public class MainActivity extends AppCompatActivity {

    int pagina;
    List<RepoInfo> repos;
    RecyclerViewAdapter rvAdapter;
    String urlPulls;






    public void carregaDadosDaApi(int page) {

        //Log.i("---Página que vem", Integer.toString(page));
        String url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + Integer.toString(page);

        new BaixaDadosTask().execute(url);

    }


    private class BaixaDadosTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... urls) {
            String resposta = "";
            for (String url : urls){
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);

                try {
                    HttpResponse executa = httpClient.execute(httpGet);
                    InputStream conteudo = executa.getEntity().getContent();

                    BufferedReader buffer = new BufferedReader(new InputStreamReader(conteudo));
                    String s = "";
                    while ((s = buffer.readLine()) != null){
                        resposta += s;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return resposta;

        }
        @Override
        protected void onPostExecute(String resultado){
           // Log.i("Dados resultado ", resultado);
            parseJSON(resultado);


        }
    }

    public void parseJSON(String json){

        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, RepoInfo.class);
        JsonAdapter<List<RepoInfo>> adapter = moshi.adapter(type);


        try {
            JSONObject jsonObject = new JSONObject(json);
            String jsonFinal = jsonObject.getString("items");
            //Log.i("Conteúdo jsonFinal", jsonFinal);



            if (rvAdapter.getItemCount()==0){
                repos.addAll(adapter.fromJson(jsonFinal));
                pagina++;
                //rvAdapter.notifyItemRangeInserted(rvAdapter.getItemCount(),repos.size()-1);
                rvAdapter.notifyDataSetChanged();

            } else {
                List<RepoInfo> proximaPagina = new ArrayList<>();
                proximaPagina.addAll(adapter.fromJson(jsonFinal));
                repos.addAll(proximaPagina);
                proximaPagina.clear();
                rvAdapter.notifyItemRangeInserted(rvAdapter.getItemCount(),repos.size()-1);
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("GitHub JavaPop");

        myToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        myToolbar.setTitleTextColor(Color.WHITE);
        myToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));

        repos = new ArrayList<>();

        if (repos.size()==0){
            pagina = 1;
        }
        carregaDadosDaApi(pagina);


        RecyclerView reposRecyclerView = (RecyclerView) findViewById(R.id.repoRecyclerView);
        rvAdapter = new RecyclerViewAdapter(repos);
        reposRecyclerView.setAdapter(rvAdapter);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        reposRecyclerView.setLayoutManager(linearLayoutManager);


        RecycleClick.addTo(reposRecyclerView).setOnItemClickListener(new RecycleClick.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                urlPulls = "https://api.github.com/repos/"+repos.get(position).getFull_name()+"/pulls";
                //Log.i("Url", urlPulls);

                Intent intent = new Intent(getApplicationContext(), PullRequestsListActivity.class);
                intent.putExtra("URL Pulls", urlPulls);
                intent.putExtra("namerepo", repos.get(position).getName());

                startActivity(intent);

            }
        });


        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, final int totalItemsCount, RecyclerView view) {

                pagina = page;
                carregaDadosDaApi(pagina);
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        rvAdapter.notifyItemRangeInserted(rvAdapter.getItemCount(),repos.size()-1);

                    }
                });
            }
        };

        reposRecyclerView.addOnScrollListener(scrollListener);
    }



}

