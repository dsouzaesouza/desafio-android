package br.com.theoldpinkeye.desafioconcrete;

import android.app.Activity;
import android.os.Bundle;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;


/**
 * Created by Dante Souza e Souza on 05/10/2017.
 */

public class RepositoryItem extends Activity {

    TextView repoName;
    TextView description;
    TextView ownerName;
    TextView forksNumber;
    TextView starsNumber;
    ImageView starIcon;
    ImageView forkIcon;
    ImageView ownerAvatar;





    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.repo_item_layout);
        repoName = (TextView) findViewById(R.id.repoName);
        description = (TextView) findViewById(R.id.repoDescription);
        ownerName = (TextView) findViewById(R.id.ownerTextView);
        forksNumber = (TextView) findViewById(R.id.forksTextView);
        starsNumber = (TextView) findViewById(R.id.starsTextView);
        starIcon = (ImageView) findViewById(R.id.starIcon);
        forkIcon = (ImageView) findViewById(R.id.forkIcon);
        ownerAvatar = (ImageView) findViewById(R.id.ownerPhoto);

        Glide.with(this).load("http://goo.gl/gEgYUd").into(ownerAvatar);





    }

}
