package br.com.theoldpinkeye.desafioconcrete;

/**
 * Created by Dante Souza e Souza on 05/10/2017.
 */

public class RepoInfo {

    private String name;

    private String full_name;
    private String html_url;

    private String description;
    private int forks_count;
    private int stargazers_count;
    private int watchers_count;

    Owner owner;


    public Owner getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }


    public String getFull_name() {
        return full_name;
    }

    public String getHtml_url() {
        return html_url;
    }


    public String getDescription() {
        return description;
    }

    public int getForks_count() {
        return forks_count;
    }

    public int getWatchers_count() {
        return watchers_count;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }
}
