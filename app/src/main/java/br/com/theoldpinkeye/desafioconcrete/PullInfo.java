package br.com.theoldpinkeye.desafioconcrete;

/**
 * Created by Dante Souza e Souza on 07/10/2017.
 */

public class PullInfo {

    private String html_url;
    private String title;
    private String body;
    private String created_at;

    Owner user;


    public String getHtmlUrl() {
        return html_url;
    }
    public String getBody() {
        return body;
    }
    public String getTitle() {
        return title;
    }
    public String getCreated_at() {
        return created_at;
    }
    public Owner getUser() {
        return user;
    }
}
