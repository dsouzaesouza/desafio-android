package br.com.theoldpinkeye.desafioconcrete;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.chootdev.recycleclick.RecycleClick;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PullRequestsListActivity extends AppCompatActivity {

    List<PullInfo> pulls;
    PullsRecyclerViewAdapter pullsAdapter;
    String sitePull;
    Intent intent;
    TextView openedPulls;
    TextView closedPulls;
    TextView slashTextView;
    TextView clTextView;
    TextView opTextView;






    public void carregaDadosDaApi(String url) {

        //Log.i("---Página que vem", url);
        new TarefaBaixaDados().execute(url);

    }




    private class TarefaBaixaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String resposta = "";
            for (String url : urls){
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);

                try {
                    HttpResponse executa = httpClient.execute(httpGet);
                    InputStream conteudo = executa.getEntity().getContent();

                    BufferedReader buffer = new BufferedReader(new InputStreamReader(conteudo));
                    String s = "";
                    while ((s = buffer.readLine()) != null){
                        resposta += s;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return resposta;

        }
        @Override
        protected void onPostExecute(String resultado){
            //Log.i("Dados resultado ", resultado);
            parseJSON(resultado);


        }
    }

    public void parseJSON(String json){

        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, PullInfo.class);
        JsonAdapter<List<PullInfo>> mAdapter = moshi.adapter(type);


        try {


           // Log.i("Conteúdo jsonFinal", json);
            pulls.addAll(mAdapter.fromJson(json));

            //Log.i("Tamanho lista pulls", Integer.toString(pulls.size()));
            //pullsAdapter.notifyItemRangeInserted(pullsAdapter.getItemCount(),pulls.size()-1);
            pullsAdapter.notifyDataSetChanged();

            openedPulls.setText(Integer.toString(pulls.size()));
            slashTextView.setVisibility(View.VISIBLE);
            clTextView.setVisibility(View.VISIBLE);
            opTextView.setVisibility(View.VISIBLE);
            closedPulls.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests_list);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("GitHub JavaPop");

        myToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        myToolbar.setTitleTextColor(Color.WHITE);
        myToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));

        openedPulls = (TextView) findViewById(R.id.openedTextView);
        closedPulls = (TextView) findViewById(R.id.closedTextView);
        slashTextView = (TextView) findViewById(R.id.slashTextView);
        clTextView = (TextView) findViewById(R.id.clTextView);
        opTextView = (TextView) findViewById(R.id.opTextView);




        intent = getIntent();

        myToolbar.setTitle(intent.getExtras().getString("namerepo"));

        String urlPulls = intent.getExtras().getString("URL Pulls");

        pulls = new ArrayList<>();
        //Log.i("pulls Size", Integer.toString(pulls.size()));
        carregaDadosDaApi(urlPulls);


        RecyclerView pullsRecyclerView = (RecyclerView) findViewById(R.id.pullsRecyclerView);
        pullsAdapter = new PullsRecyclerViewAdapter(pulls);
        pullsRecyclerView.setAdapter(pullsAdapter);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        pullsRecyclerView.setLayoutManager(linearLayoutManager);


        RecycleClick.addTo(pullsRecyclerView).setOnItemClickListener(new RecycleClick.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                sitePull = pulls.get(position).getHtmlUrl();
               // Log.i("Url", sitePull);

                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(sitePull)));

            }
        });



        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, final int totalItemsCount, RecyclerView view) {


                view.post(new Runnable() {
                    @Override
                    public void run() {

                        pullsAdapter.notifyItemRangeInserted(pullsAdapter.getItemCount(),pulls.size()-1);


                    }
                });
            }
        };


        pullsRecyclerView.addOnScrollListener(scrollListener);
    }
        
        
        
        
        
}

