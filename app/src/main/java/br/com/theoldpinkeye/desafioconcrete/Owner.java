package br.com.theoldpinkeye.desafioconcrete;

/**
 * Created by Dante Souza e Souza on 05/10/2017.
 */

public class Owner {

    private String login;
    private String avatar_url;

    public String getLogin() {
        return login;
    }
    public String getAvatar_url() {
        return avatar_url;
    }
}
