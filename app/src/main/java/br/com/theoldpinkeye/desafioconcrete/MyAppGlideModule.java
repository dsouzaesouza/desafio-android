package br.com.theoldpinkeye.desafioconcrete;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Dante Souza e Souza on 05/10/2017.
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}
